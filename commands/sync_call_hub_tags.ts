import { inject } from '@adonisjs/core'
import { BaseCommand } from '@adonisjs/core/ace'
import type { CommandOptions } from '@adonisjs/core/types/ace'
import logger from '@adonisjs/core/services/logger'
import CallHubService from '#services/callhub_service'

// noinspection JSUnusedGlobalSymbols
export default class SyncCallHubTags extends BaseCommand {
  static commandName = 'ch:sync-tags'
  static description = 'Synchronizes CallHub Tags from the API to the database'

  static options: CommandOptions = {
    startApp: true,
  }

  @inject()
  async run(ch: CallHubService) {
    logger.info(`BEGIN CallHub tags sync`)
    await ch.tagsSync()
    logger.info(`END CallHub tags sync`)
  }
}
