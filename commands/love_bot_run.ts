import { inject } from '@adonisjs/core'
import { BaseCommand } from '@adonisjs/core/ace'
import type { CommandOptions } from '@adonisjs/core/types/ace'
import bolt from '@slack/bolt'
import config from '#config/slack'
import LoveBotService from '#services/love_bot/love_bot_service'
import env from '#start/env'

// noinspection JSUnusedGlobalSymbols
@inject()
export default class LoveBotRun extends BaseCommand {
  static commandName = 'run:love-bot'
  static description = 'Runs the Love Bot for Slack'

  static options: CommandOptions = {
    startApp: true,
    staysAlive: true,
  }

  @inject()
  async run(service: LoveBotService) {
    const port = Number(env.get('PORT', 3001))
    const app = new bolt.App({
      appToken: config.appToken,
      logLevel: config.logLevel,
      signingSecret: config.signingSecret,
      socketMode: true,
      token: config.token,
      port,
    })

    await service.register(app)

    await app.start(port)

    this.logger.info('⚡️ Love Bot is running!')
  }
}
