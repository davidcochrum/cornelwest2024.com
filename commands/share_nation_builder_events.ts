import { inject } from '@adonisjs/core'
import { BaseCommand } from '@adonisjs/core/ace'
import { Logger } from '@adonisjs/core/logger'
import { CommandOptions } from '@adonisjs/core/types/ace'
import ProcessedVolunteerEvent from '#models/processed_volunteer_event'
import NationBuilderService, { Event, EventsGetResponse } from '#services/nation_builder_service'
import SlackService from '#services/slack_service'

// noinspection JSUnusedGlobalSymbols
export default class ShareNationBuilderEvents extends BaseCommand {
  static commandName = 'nb:share-events'
  static description = `Shares newly approved volunteer events from NationBuilder in the corresponding state's Slack channel`

  static options: CommandOptions = {
    startApp: true,
  }

  @inject()
  async run(logger: Logger, nb: NationBuilderService, slack: SlackService) {
    logger.info(`BEGIN processing volunteer events`)
    let eventsResponse: EventsGetResponse | null = null
    let events: Event[] = []
    do {
      eventsResponse = await nb.getVolunteerEvents({}, eventsResponse?.next)
      events.push(...eventsResponse.results)
    } while (eventsResponse.next)

    logger.debug(`Fetched ${events.length} events`)
    await Promise.all(
      events.map(async (event: Event) => {
        const processed = await ProcessedVolunteerEvent.find(event.id)
        if (processed) {
          logger.debug(`Already processed event #${event.id}`)
          return
        }

        logger.debug(event, `Posting event #${event.id}`)
        await slack.postNationBuilderEvent(event, `New volunteer event: ${event.headline}`)

        await ProcessedVolunteerEvent.create({ id: event.id, headline: event.headline })
      })
    )
    logger.info(`END processing volunteer events`)
  }
}
