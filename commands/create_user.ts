import { BaseCommand } from '@adonisjs/core/ace'
import { CommandOptions } from '@adonisjs/core/types/ace'
import { faker } from '@faker-js/faker'
import User from '#models/user'

export default class CreateUser extends BaseCommand {
  static commandName = 'user:create'
  static description = 'Creates a new user'
  static options: CommandOptions = {
    startApp: true,
  }

  async run() {
    const fullName = await this.prompt.ask('Enter full name', {
      validate(value) {
        return value.length < 2
          ? 'Full name must be at least 2 characters long'
          : true
      },
    })
    const email = await this.prompt.ask('Enter email', {
      async validate(value) {
        if (value.length < 3 || !value.includes('@')) {
          return 'Email must be valid'
        }
        if (await User.findBy('email', value)) {
          return 'Email in use'
        }
        return true
      },
    })
    let password = await this.prompt.secure('Enter a password (leave blank to generate one)', {
      validate(value) {
        return value.length > 0 && value.length < 8
          ? 'Password must be at least 8 characters long'
          : true
      },
    })
    if (password.length < 1) {
      password = faker.internet.password()
    }

    const user = await User.create({ fullName, email, password })
    this.logger.info(`User created:\n${JSON.stringify(user.serialize(), null, 2)}\npassword: ${password}`)
  }
}
