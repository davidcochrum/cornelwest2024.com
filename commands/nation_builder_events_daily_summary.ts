import { inject } from '@adonisjs/core'
import { BaseCommand } from '@adonisjs/core/ace'
import { Logger } from '@adonisjs/core/logger'
import { CommandOptions } from '@adonisjs/core/types/ace'
import NationBuilderService, { Event, EventsGetResponse } from '#services/nation_builder_service'
import SlackService from '#services/slack_service'
import { DateTime } from 'luxon'

// noinspection JSUnusedGlobalSymbols
export default class NationBuilderEventsDailySummary extends BaseCommand {
  static commandName = 'nb:events-daily-summary'
  static description = `Posts a daily summary of approved volunteer events from NationBuilder in the corresponding state's Slack channel`

  static options: CommandOptions = {
    startApp: true,
  }

  /** The hour at which to post the daily summary. */
  summaryHour = 9

  @inject()
  async run(logger: Logger, nb: NationBuilderService, slack: SlackService) {
    if (DateTime.local({ zone: 'America/New_York' }).hour < this.summaryHour) {
      logger.info('Too early to process any US daily summaries')
      return
    }
    if (DateTime.local({ zone: 'Pacific/Honolulu' }).hour > this.summaryHour) {
      logger.info('Too late to process US daily summaries')
      return
    }

    const nowUTC = DateTime.now()
    const filterOffset = `-${String(nowUTC.hour - this.summaryHour).padStart(2, '0')}:00`
    const startingStr = `${nowUTC.toISODate()}T${String(this.summaryHour).padStart(2, '0')}:00:00${filterOffset}`
    const starting = DateTime.fromISO(startingStr, { setZone: true })
    const until = starting.plus({ days: 1 }).minus({ second: 1 })
    logger.info(
      `BEGIN processing volunteer events daily summary for ${starting.toISODate()} within hour ${this.summaryHour} matching offset ${filterOffset}`,
    )
    let eventsResponse: EventsGetResponse | null = null
    let events: Event[] = []
    do {
      eventsResponse = await nb.getVolunteerEvents(
        { starting: String(starting.toISO()), until: String(until.toISO()) },
        eventsResponse?.next,
      )
      eventsResponse.results.forEach((event) => {
        // keep events where it's currently within the summary hour in the event's timezone
        const eventTimezone = nb.normalizeTimezone(event.time_zone)
        const nowLocal = DateTime.now().setZone(eventTimezone)
        if (nowLocal.hour === this.summaryHour) {
          events.push(event)
        }
      })
    } while (eventsResponse.next)

    logger.debug(`Matched ${events.length} events`)
    await Promise.all(
      events.map(async (event: Event) => {
        logger.debug(event, `Posting event #${event.id}`)
        await slack.postNationBuilderEvent(
          event,
          `Volunteer event happening today: ${event.headline}`,
        )
      })
    )
    logger.info(`END processing volunteer events daily summary`)
  }
}
