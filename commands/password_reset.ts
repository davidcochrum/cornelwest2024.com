import { BaseCommand } from '@adonisjs/core/ace'
import hash from '@adonisjs/core/services/hash'
import { CommandOptions } from '@adonisjs/core/types/ace'
import { faker } from '@faker-js/faker'
import User from '#models/user'

export default class PasswordReset extends BaseCommand {
  static commandName = 'user:password-reset'
  static description = 'Resets the password for a user'
  static options: CommandOptions = {
    startApp: true,
    staysAlive: false,
  }


  async run() {
    const email = await this.prompt.ask('Enter email', {
      async validate(value) {
        if (await User.findBy('email', value)) {
          return true
        }
        return 'No user found'
      },
    })
    let password = (await this.prompt.secure('Enter a password (leave blank to generate one)', {
      validate(value) {
        return value.length > 0 && value.length < 8
          ? 'Password must be at least 8 characters long'
          : true
      },
    }))
    if (password.length < 1) {
      password = faker.internet.password()
    }

    const user = await User.findByOrFail('email', email)
    user.password = await hash.use('scrypt').make(password.trim())
    await user.save()
    this.logger.info(`User password updated:\n${JSON.stringify(user.serialize(), null, 2)}\npassword: ${password}`)
  }
}
