import { DateTime } from 'luxon'
import { afterFetch, afterFind, BaseModel, beforeSave, column } from '@adonisjs/lucid/orm'

export enum BallotAccessStatus {
  NotOpen = 'not-open',
  InProgress = 'in-progress',
  NeedElectors = 'need-electors',
  OnBallot = 'on-ballot',
  NoWriteIn = 'no-write-in',
}

export default class BallotAccessState extends BaseModel {
  @column({ isPrimary: true })
  declare code: string

  @column()
  declare name: string

  @column()
  declare channel: string | null

  @column({
    serialize(value) {
      if (Array.isArray(value)) {
        return value
      }
      return JSON.parse(value ?? '[]')
    },
  })
  declare matchWords: string[]

  @beforeSave()
  static async jsonifyMatchWords(model: BallotAccessState) {
    // @ts-ignore
    model.matchWords = JSON.stringify(model.matchWords ?? [])
  }

  @afterFetch()
  @afterFind()
  static async parseMatchWords(model: BallotAccessState) {
    // @ts-ignore
    if (!Array.isArray(model.matchWords)) {
      model.matchWords = JSON.parse(model.matchWords ?? '[]')
    }
  }

  @column()
  declare signaturesRequired: number | null

  @column.date()
  declare startDate: DateTime | null

  @column.date()
  declare deadlineDate: DateTime | null

  @column()
  declare note: string | null

  @column()
  declare campaignLink: string | null

  @column()
  declare stateLink: string | null

  @column()
  declare status: BallotAccessStatus

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime
}
