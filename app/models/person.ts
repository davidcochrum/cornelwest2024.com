import { DateTime } from 'luxon'
import { BaseModel, column } from '@adonisjs/lucid/orm'

export default class Person extends BaseModel {
  // noinspection JSUnusedGlobalSymbols
  static selfAssignPrimaryKey = true
  static tableName = 'people'

  /** Person ID within NationBuilder */
  @column({ isPrimary: true })
  declare id: number | null

  /** Contact ID within CallHub */
  @column()
  declare chId: string | null

  @column()
  declare email: string | null

  @column()
  declare firstName: string | null

  @column()
  declare lastName: string | null

  @column()
  declare doNotCall: boolean

  @column()
  declare doNotText: boolean

  @column()
  declare mobile: string | null

  @column()
  declare phone: string | null

  @column({
    consume(value) {
      if (Array.isArray(value)) {
        return value
      }
      return JSON.parse(value || '[]')
    },
    prepare(value) {
      if (Array.isArray(value)) {
        return JSON.stringify(value || [])
      }
      return String(value)
    },
  })
  declare tags: string[]

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime | null
}
