import { DateTime } from 'luxon'
import { BaseModel, column } from '@adonisjs/lucid/orm'

// noinspection JSUnusedGlobalSymbols
export default class ProcessedVolunteerEvent extends BaseModel {
  static selfAssignPrimaryKey = true

  @column({ isPrimary: true })
  declare id: number

  @column()
  declare headline: string | null

  @column.dateTime({ autoCreate: true })
  declare at: DateTime
}
