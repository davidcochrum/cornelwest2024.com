import { DateTime } from 'luxon'
import { BaseModel, column } from '@adonisjs/lucid/orm'

export default class CHTag extends BaseModel {
  // noinspection JSUnusedGlobalSymbols
  static selfAssignPrimaryKey = true
  static tableName = 'ch_tags'

  @column({ isPrimary: true })
  declare pk_str: string

  @column()
  declare name: string

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime
}
