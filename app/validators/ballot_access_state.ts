import vine from '@vinejs/vine'
import { BallotAccessStatus } from '#models/ballot_access_state'
import { DateTime } from 'luxon'

const transformDate = (value: Date | null) => (value ? DateTime.fromJSDate(value) : null)

/**
 * Validates the ballot access state's update action
 */
export const updateBallotAccessStateValidator = vine.compile(
  vine.object({
    channel: vine.string().trim().minLength(2).optional().nullable(),
    signaturesRequired: vine.number().optional().nullable(),
    startDate: vine.date().optional().nullable().transform(transformDate),
    deadlineDate: vine.date().optional().nullable().transform(transformDate),
    note: vine.string().optional().nullable(),
    campaignLink: vine.string().optional().nullable(),
    stateLink: vine.string().optional().nullable(),
    status: vine.enum(Object.values(BallotAccessStatus)).optional(),
  })
)
