import { inject } from '@adonisjs/core'
import { fsImportAll } from '@adonisjs/core/helpers'
import { Logger } from '@adonisjs/core/logger'
import app from '@adonisjs/core/services/app'
import bolt from '@slack/bolt'
import SlackRegister from '#services/love_bot/slack_register_interface'

@inject()
export default class LoveBotService {
  constructor(private logger: Logger) {}

  async register(slackApp: bolt.App) {
    await this.registerAll(slackApp, './action', '⚡')
    await this.registerAll(slackApp, './command', '🪄')
    await this.registerAll(slackApp, './event', '⏰')
    await this.registerAll(slackApp, './message', '👂')
    await this.registerAll(slackApp, './shortcut', '️✂️')
    await this.registerAll(slackApp, './view', '👓')
  }

  async registerAll(slackApp: bolt.App, path: string, icon: string) {
    const handlers = await fsImportAll(new URL(path, import.meta.url))
    for (const [filename, inst] of Object.entries(handlers)) {
      const handler: SlackRegister = await app.container.make(inst)
      handler.register(slackApp)
      // @ts-ignore
      this.logger.debug(`${icon} ${inst.name ?? `${path}/${filename}`} registered`)
    }
  }
}
