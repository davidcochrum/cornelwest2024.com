import logger from '@adonisjs/core/services/logger'
import bolt from '@slack/bolt'
import { WebClient } from '@slack/web-api'
import BallotAccessState from '#models/ballot_access_state'

export default class SlackUtils {
  private static _channelNameIdCache: Map<string, string>

  static get channelNameIdCache(): Map<string, string> {
    if (!this._channelNameIdCache) {
      this._channelNameIdCache = new Map<string, string>()
    }
    return this._channelNameIdCache
  }

  /**
   * Attempts to retrieve a private channel ID matching a name.
   */
  // @ts-ignore
  static async getPrivateChannelId(client: WebClient, channelName: string): Promise<string | null> {
    if (this.channelNameIdCache.has(channelName)) {
      return <string>this._channelNameIdCache.get(channelName)
    }

    let resp: any | null = null
    let matchedChannelId = null
    do {
      try {
        resp = await client.conversations.list({ types: 'private_channel' })
      } catch (e) {
        logger.warn(e, 'unable to retrieve private channels')
      }
      for (const channel of resp.channels) {
        this.channelNameIdCache.set(<string>channel.name, <string>channel.id)
        logger.debug('cached channel %s -> %s', channel.name, channel.id)
        if ((channel.name ?? '') === channelName) {
          matchedChannelId = channel.id
        }
      }
    } while (resp.response_metadata.next_cursor)
    return matchedChannelId
  }

  // @ts-ignore
  static async userIsInChannel(client: bolt.Client, channel: string, userId: string) {
    const { members } = await client.conversations.members({ channel })
    return -1 !== members.indexOf(userId)
  }

  static async getStateChannelMap() {
    const states = await BallotAccessState.all()
    return new Map(
      states.map((state) => {
        const stateData = state.serialize()
        return [
          stateData.name,
          {
            stateCode: stateData.code,
            channelName: stateData.channel,
            matchWords: stateData.matchWords,
          },
        ]
      })
    )
  }
}
