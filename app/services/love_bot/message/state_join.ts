import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import bolt from '@slack/bolt'
// @ts-ignore
import noBotMessages from '#services/love_bot/middleware/no_bot_messages'
import SlackRegister from '#services/love_bot/slack_register_interface'
import SlackUtils from '#services/love_bot/slack_utils'
import StateJoinMessageAction from '#services/love_bot/action/state_join_message'

@inject()
export default class StateJoinMessage implements SlackRegister {
  messageIgnoreRegex = new RegExp(/\b(zoom)\b/, 'i')

  constructor(private logger: Logger) {}

  // @ts-ignore
  async joinRequestHandler(params) {
    const { client, message, say } = params
    const messageText = String(message.text)
    this.logger.info(`Matched join request within message: %s`, messageText)
    this.logger.debug(params, 'join request message params')

    if (this.messageIgnoreRegex.exec(messageText)) {
      this.logger.debug('message matched ignore regex')
      return
    }

    for (const [stateName, { channelName, matchWords }] of await SlackUtils.getStateChannelMap()) {
      if (!channelName) {
        this.logger.warn('No channel configured for %s', stateName)
        continue
      }

      if (!(new RegExp(`\\b(${matchWords.join('|')})\\b`, 'gi').exec(messageText))) {
        this.logger.debug(`Message didn't match words: %s`, matchWords.join(', '))
        continue
      }

      const channelId = await SlackUtils.getPrivateChannelId(client, channelName)
      if (!channelId) {
        this.logger.warn(params, 'Unable to locate private channel matching #%s', channelName)
        continue
      }

      if (await SlackUtils.userIsInChannel(client, channelId, message.user)) {
        this.logger.info('User @%s already in #%s', message.user, channelName)
        continue
      }

      const sayText = `Hey there <@${message.user}>, did I understand correctly that you wish to join the ${stateName} channel?`
      await say({
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: sayText,
            },
          },
          {
            block_id: StateJoinMessageAction.blockIdAdd,
            type: 'actions',
            elements: [
              {
                action_id: StateJoinMessageAction.actionIdAddYes,
                type: 'button',
                text: {
                  type: 'plain_text',
                  text: 'Yes',
                },
                style: 'primary',
                value: channelName,
              },
              {
                action_id: StateJoinMessageAction.actionIdAddNo,
                type: 'button',
                text: {
                  type: 'plain_text',
                  text: 'No',
                },
                style: 'danger',
                value: channelName,
              },
            ],
          },
        ],
        response_type: 'in_channel',
        replace_original: false,
        text: sayText,
        thread_ts: message.ts,
      })
      return
    }
    this.logger.debug('Unable to match a state within the message')
  }

  register(slackApp: bolt.App) {
    slackApp.message(
      new RegExp(/\b(add|added|invite|invited|join|joining)\b/, 'i'),
      (params) => noBotMessages(params),
      (params) => this.joinRequestHandler(params)
    )
  }
}
