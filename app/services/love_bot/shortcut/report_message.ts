import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import bolt from '@slack/bolt'
// @ts-ignore
import { WebClient } from '@slack/web-api'
import SlackRegister from '#services/love_bot/slack_register_interface'
import ReportMessageSubmitView from '#services/love_bot/view/report_message_submit'

// noinspection JSUnusedGlobalSymbols
@inject()
export default class ReportMessageShortcut implements SlackRegister {
  constructor(private logger: Logger) {}

  /**
   * Handles the report message shortcut.
   * @param {bolt.AckFn} params.ack
   * @param {WebClient} params.client
   * @param {bolt.MessageShortcut} params.shortcut
   */
  // @ts-ignore
  async handler(params) {
    const { ack, client, shortcut } = params
    await ack()
    this.logger.info(
      `Reported message (${shortcut.message_ts}) from ${shortcut.user.username} shortcut being handled by opening confirmation modal`
    )
    this.logger.debug(params, 'report message shortcut params')

    await client.views.open({
      trigger_id: shortcut.trigger_id,
      view: {
        type: 'modal',
        callback_id: ReportMessageSubmitView.callbackId,
        private_metadata: JSON.stringify(shortcut),
        title: {
          type: 'plain_text',
          text: 'Report Offensive Message',
        },
        close: {
          type: 'plain_text',
          text: 'Cancel',
        },
        submit: {
          type: 'plain_text',
          text: 'Report',
        },
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: 'Please review the <https://docs.google.com/document/d/1x-wK5owwE6gFuBgkxKGBvlH8_lDuGvLBbwQuDKM3uQQ/edit?usp=sharing|Community Norms> and confirm whether you would like to report the message below as a violation to the Love Mod team:',
            },
          },
          {
            type: 'input',
            label: {
              type: 'plain_text',
              text: 'Reason for reporting?',
            },
            element: {
              action_id: ReportMessageSubmitView.reasonActionId,
              type: 'plain_text_input',
              multiline: true,
            },
          },
          {
            type: 'divider',
          },
          {
            type: 'context',
            elements: [
              {
                type: 'mrkdwn',
                text: shortcut.message.text,
              },
            ],
          },
        ],
      },
    })
  }

  register(slackApp: bolt.App) {
    slackApp.shortcut('report_message', (params) => this.handler(params))
  }
}
