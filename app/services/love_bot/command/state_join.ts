import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import bolt from '@slack/bolt'
import SlackRegister from '#services/love_bot/slack_register_interface'
import SlackUtils from '#services/love_bot/slack_utils'
import StateJoinCommandAction from '#services/love_bot/action/state_join_command'

@inject()
export default class StateJoinCommand implements SlackRegister {
  constructor(private logger: Logger) {}

  // @ts-ignore
  async handleCommand(params) {
    const { client, command, ack, respond } = params
    await ack()
    this.logger.info('state join command invoked')
    this.logger.debug(params, 'state join command params')
    for (const [stateName, { stateCode, channelName }] of await SlackUtils.getStateChannelMap()) {
      if (!channelName) {
        this.logger.warn('No channel configured for %s', stateName)
        await respond(
          `Sorry, we haven't setup a channel for ${stateName}. Please check back later.`
        )
        return
      }
      const stateInputLower = command.text.toLowerCase()
      if (stateInputLower === stateName.toLowerCase() || command.text === stateCode.toLowerCase()) {
        const channelId = await SlackUtils.getPrivateChannelId(client, channelName)
        if (!channelId) {
          this.logger.warn(params, 'Unable to locate private channel matching #%s', channelName)
          await respond(
            `Sorry, I'm having trouble finding the channel for ${stateName}. Please check back later.`
          )
          return
        }

        if (await SlackUtils.userIsInChannel(client, channelId, command.user_id)) {
          this.logger.info('User @%s already in #%s', command.user_name, channelName)
          await respond(`It looks like you've already joined the <#${channelId}> channel`)
          return
        }

        const sayText = `Hey there <@${command.user_id}>, did I understand correctly that you wish to join the ${stateName} channel?`
        await respond({
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: sayText,
              },
            },
            {
              block_id: StateJoinCommandAction.blockIdAdd,
              type: 'actions',
              elements: [
                {
                  action_id: StateJoinCommandAction.actionIdAddYes,
                  type: 'button',
                  text: {
                    type: 'plain_text',
                    text: 'Yes',
                  },
                  style: 'primary',
                  value: channelName,
                },
                {
                  action_id: StateJoinCommandAction.actionIdAddNo,
                  type: 'button',
                  text: {
                    type: 'plain_text',
                    text: 'No',
                  },
                  style: 'danger',
                  value: channelName,
                },
              ],
            },
          ],
        })
        return
      }
    }
    this.logger.warn('Unable to match the state parameter: %s', command.text)
    await respond(`Sorry, I'm unable to find a match for ${command.text}. Please try again.`)
  }

  register(slackApp: bolt.App): void {
    slackApp.command('/state-join', (params) => this.handleCommand(params))
  }
}
