import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import bolt from '@slack/bolt'
// @ts-ignore
import { WebClient } from '@slack/web-api'
import SlackRegister from '#services/love_bot/slack_register_interface'
import SlackUtils from '#services/love_bot/slack_utils'

// noinspection JSUnusedGlobalSymbols
@inject()
export default class ReportMessageSubmitView implements SlackRegister {
  static callbackId = 'report_message_confirm'
  static reasonActionId = 'report_message_reason'

  constructor(private logger: Logger) {}

  /**
   * Handles the report message shortcut.
   * @param {bolt.AckFn} params.ack
   * @param {WebClient} params.client
   * @param {bolt.ModalView} params.view
   */
  // @ts-ignore
  async handler(params) {
    const { ack, body, client, view } = params
    const shortcut = JSON.parse(view.private_metadata)
    this.logger.info(`${body.user.id} confirmed intent to reported message (${body.trigger_id})`)
    this.logger.debug(params, 'report message confirmation params')

    const channel = await SlackUtils.getPrivateChannelId(client, 'love-mod-team')
    const values = Object.values(view.state.values)
    // @ts-ignore
    const reason = values[0]?.report_message_reason?.value ?? 'Unknown'
    const messageLink = `https://${shortcut.team.domain}.slack.com/archives/${shortcut.channel.id}/p${shortcut.message_ts.replace('.', '')}`
    try {
      await client.chat.postMessage({
        channel,
        text: `:bangbang: New offensive message report from <@${body.user.id}>`,
        blocks: [
          {
            type: 'header',
            text: {
              type: 'plain_text',
              text: 'Offensive Message Report',
            },
          },
          {
            type: 'section',
            fields: [
              {
                type: 'mrkdwn',
                text: `*Reported By*\n<@${body.user.id}>`,
              },
              {
                type: 'mrkdwn',
                text: `*Reason*\n${reason}`,
              },
            ],
          },
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `<${messageLink}|View Message>`,
            },
          },
        ],
      })

      await ack({
        response_action: 'update',
        view: {
          type: 'modal',
          title: {
            type: 'plain_text',
            text: 'Report Offensive Message',
          },
          close: {
            type: 'plain_text',
            text: 'Close',
          },
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: ':white_check_mark: The message has been reported to the Love Mod team.',
              },
            },
          ],
        },
      })
    } catch (e) {
      this.logger.error(e, 'Unable to report message')
    }
  }

  register(slackApp: bolt.App) {
    slackApp.view(ReportMessageSubmitView.callbackId, (params) => this.handler(params))
  }
}
