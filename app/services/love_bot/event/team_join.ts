import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import bolt from '@slack/bolt'
import { WebClient } from '@slack/web-api'
import SlackRegister from '#services/love_bot/slack_register_interface'

@inject()
export default class TeamJoinEvent implements SlackRegister {
  constructor(private logger: Logger) {}

  // @ts-ignore
  async handle({ client, event }: { client: WebClient; event: bolt.TeamJoinEvent }) {
    this.logger.info('team join event invoked')
    this.logger.debug(event, 'team join event params')

    await client.chat.postMessage({
      channel: event.user.id,
      text: `Welcome to the Cornel West 2024 Slack Workspace! We're so happy you've joined us.

1. You can access this Slack workspace either on a desktop or mobile app (download is free).
2. If you haven't joined SocialRoots yet, please join the main group here and join your respective subgroups:
   https://socialroots.wicked.coop/invitation?g=db79a3f48f0511eeb3257e47a2f35955&k=5cf6c6.
   You'll want to join to receive regular updates from your state's quartet of leaders and the campaign as well.
3. Slack has an extensive amount of tutorials for users of all experience levels here:
   https://slack.com/help/categories/360000049063#tutorials
   and you can join the SocialRoots Help Desk subgroup for guides on that platform.
4. Getting started with this campaign is easy - check out this Getting Started Guide to find how you want to help!
   You can take up a leadership role in your state, you can host activities, you can petition for Dr. West in your
   state to help him appear on the general election ballot in your state (he won't be on primary ballots, as he's
   running as an independent) - but overall, we need you to contribute who you are to the campaign - and this movement
   that will go beyond! One pager here:
   https://docs.google.com/document/d/1TpCYv3vfULv6j4BxBx0RyDgYM20Ypb665wcAN37Y38w/edit?usp=sharing`,
      mrkdwn: true,
    })
  }

  register(slackApp: bolt.App): void {
    // @ts-ignore
    slackApp.event('team_join', (params) => this.handle(params))
  }
}
