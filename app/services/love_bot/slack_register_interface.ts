import bolt from '@slack/bolt'

export default interface SlackRegister {
  /**
   * Registers the message, command, action, etc. handler(s).
   * @param {bolt.App} slackApp
   */
  register(slackApp: bolt.App): void
}
