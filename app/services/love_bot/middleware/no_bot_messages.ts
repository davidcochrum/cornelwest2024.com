// @ts-ignore
const noBotMessages = async ({ message, next }) => {
  if (message?.subtype !== 'bot_message') {
    await next()
  }
}

export default noBotMessages
