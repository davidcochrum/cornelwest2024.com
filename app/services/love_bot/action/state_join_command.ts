import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import bolt from '@slack/bolt'
import SlackRegister from '#services/love_bot/slack_register_interface'
import SlackUtils from '#services/love_bot/slack_utils'

@inject()
export default class StateJoinCommandAction implements SlackRegister {
  static blockIdAdd = 'block_state_add_cmd'
  static actionIdAddYes = 'action_state_add_cmd_yes'
  static actionIdAddNo = 'action_state_add_cmd_no'

  constructor(private logger: Logger) {}

  // @ts-ignore
  async yesHandler(params) {
    const { ack, body, client, payload, respond } = params
    await ack()
    const channelName = payload.value
    this.logger.info('User @%s agreed to be added to #%s channel', body.user.username, channelName)
    this.logger.debug(params, 'stateAddYesHandler params')
    const user = body.user.id
    const channelId = await SlackUtils.getPrivateChannelId(client, channelName)
    await client.conversations.invite({ channel: channelId, users: user })
    await respond({
      replace_original: true,
      text: `You've been added to the <#${channelId}> channel!`,
    })
  }

  // @ts-ignore
  async noHandler(params) {
    const { ack, payload, respond } = params
    await ack()
    this.logger.info('User did not want to be added to #%s channel', payload.value)
    this.logger.debug(params, 'stateAddNoHandler params')
    await respond({
      replace_original: true,
      text: 'Whoops, my mistake!',
    })
  }

  register(slackApp: bolt.App) {
    slackApp.action(
      {
        action_id: StateJoinCommandAction.actionIdAddYes,
        block_id: StateJoinCommandAction.blockIdAdd,
      },
      (params) => this.yesHandler(params)
    )
    slackApp.action(
      {
        action_id: StateJoinCommandAction.actionIdAddNo,
        block_id: StateJoinCommandAction.blockIdAdd,
      },
      (params) => this.noHandler(params)
    )
  }
}
