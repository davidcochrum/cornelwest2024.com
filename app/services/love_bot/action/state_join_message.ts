import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import bolt from '@slack/bolt'
import SlackRegister from '#services/love_bot/slack_register_interface'
import SlackUtils from '#services/love_bot/slack_utils'

@inject()
export default class StateJoinMessageAction implements SlackRegister {
  static blockIdAdd = 'block_state_add_msg'
  static actionIdAddYes = 'action_state_add_msg_yes'
  static actionIdAddNo = 'action_state_add_msg_no'

  constructor(private logger: Logger) {}

  // @ts-ignore
  async stateAddRemoveActions(params) {
    const { body, client } = params
    const { message } = body
    const newMessage = {
      channel: body.channel.id,
      ts: message.ts,
      blocks: [],
      text: message.text,
    }
    await client.chat.update(newMessage)
  }

  // @ts-ignore
  async yesHandler(params) {
    const { ack, body, client, payload, say } = params
    await ack()
    const channelName = payload.value
    this.logger.info('User @%s agreed to be added to #%s channel', body.user.username, channelName)
    this.logger.debug(params, 'stateAddYesHandler params')
    const user = body.user.id
    const channelId = await SlackUtils.getPrivateChannelId(client, channelName)
    await client.conversations.invite({ channel: channelId, users: user })
    await client.reactions.add({
      channel: body.channel.id,
      name: 'white_check_mark',
      timestamp: body.container.thread_ts,
    })
    await this.stateAddRemoveActions({ body, client })
    await say({
      response_type: 'in_channel',
      replace_original: false,
      text: `<@${user}>, you've been added to the <#${channelId}> channel!`,
      thread_ts: body.message.ts,
    })
  }

  // @ts-ignore
  async noHandler(params) {
    const { ack, body, client, payload, say } = params
    await ack()
    this.logger.info('User did not want to be added to #%s channel', payload.value)
    this.logger.debug(params, 'stateAddNoHandler params')
    await this.stateAddRemoveActions({ body, client })
    await say({
      response_type: 'in_channel',
      replace_original: false,
      text: 'Whoops, my mistake!',
      thread_ts: body.message.ts,
    })
  }

  register(slackApp: bolt.App) {
    slackApp.action(
      {
        action_id: StateJoinMessageAction.actionIdAddYes,
        block_id: StateJoinMessageAction.blockIdAdd,
      },
      (params) => this.yesHandler(params)
    )
    slackApp.action(
      {
        action_id: StateJoinMessageAction.actionIdAddNo,
        block_id: StateJoinMessageAction.blockIdAdd,
      },
      (params) => this.noHandler(params)
    )
  }
}
