import axios from 'axios'
import call_hub from '#config/call_hub'
import CHTag from '#models/ch_tag'

interface PaginatedParams {
  page?: number
}

interface PaginatedResponse {
  count: number
  next: string | null
  previous: string | null
  results: Array<any>
}

interface ContactsParams extends PaginatedParams {
  search?: string
}

export interface Contact {
  url: string
  id: number
  pk_str: string
  owner: string
  phonebooks: Array<string>
  contact: string
  mobile: string
  last_name: string
  first_name: string
  country_code: string | null
  email: string
  created_date: string
  address: string
  city: string
  tags: Array<string>
  street_address_line1: string | null
  state: string
  zipcode: string | null
  company_name: string
  company_website: string | null
  job_title: string
  custom_fields: string
}

interface ContactsResponse extends PaginatedResponse {
  results: Contact[]
}

export interface ContactCreateParams {
  contact: string
  mobile?: string
  last_name?: string
  first_name?: string
  country_code?: string
  email?: string
  address?: string
  city?: string
  state?: string
  company_name?: string
  company_website?: string
  job_title?: string
}

interface Tag {
  name: string
  crm_type: number
  crm_id: number | null
  id: number
  pk_str: string
  created_date: string
}

interface TagListResponse extends PaginatedResponse {
  results: Array<Tag>
}

export default class CallHubService {
  private client
  constructor() {
    this.client = axios.create({
      headers: { Authorization: `Token ${call_hub.key}` },
      baseURL: 'https://api.callhub.io',
    })
  }

  async contacts(params: ContactsParams): Promise<ContactsResponse> {
    const { data } = await this.client.get('v1/contacts/', { params })
    return data
  }

  async contactFindByAnyPhone(...phones: Array<string | null>): Promise<Contact | null> {
    const matches: Contact[] = []
    await Promise.all(
      phones.map(async (search) => {
        if (!search) {
          return
        }
        const { results } = await this.contacts({ search })
        matches.push(...results)
      })
    )
    if (matches.length > 0) {
      return matches[0]
    }
    return null
  }

  async contactGet(id: string): Promise<Contact> {
    const { data } = await this.client.get(`v1/contacts/${id}/`)
    return data
  }

  async contactCreate(params: ContactCreateParams): Promise<Contact> {
    const { data } = await this.client.post('v1/contacts/', params)
    return data
  }

  async contactFirstOrCreate(id: string | null, params: ContactCreateParams): Promise<Contact> {
    if (id) {
      return this.contactGet(id)
    }

    const match = await this.contactFindByAnyPhone(params.contact, String(params.mobile))
    if (match) {
      return match
    }

    return this.contactCreate(params)
  }

  async contactTag(contact: { pk_str: string }, names: Array<string>): Promise<Tag[]> {
    let tags = []
    for (const name of names) {
      const tagModel = await this.tagFirstOrCreate(name)
      tags.push(tagModel.pk_str)
    }
    const { data } = await this.client.patch(`v2/contacts/${contact.pk_str}/taggings/`, { tags })
    return data
  }

  async tags(): Promise<CHTag[]> {
    return CHTag.all()
  }

  async tagCreate(tag: string): Promise<CHTag> {
    const { data } = await this.client.post(`v2/tags/`, { tag })
    return CHTag.create({ pk_str: data.pk_str, name: tag })
  }

  async tagFirstOrCreate(name: string): Promise<CHTag> {
    const tagModel = await CHTag.query().where('name', name).first()
    if (tagModel) {
      return tagModel
    }
    return this.tagCreate(name)
  }

  async tagsSync(): Promise<void> {
    let data: TagListResponse | null = null
    do {
      const response = await this.client.get(data?.next || 'v2/tags/')
      data = response.data as TagListResponse
      await CHTag.updateOrCreateMany(
        ['pk_str'],
        data.results.map(({ pk_str, name }: Tag) => ({ pk_str, name }))
      )
    } while (data.next)
  }
}
