import axios, { AxiosInstance } from 'axios'
import nbConfig from '#config/nation_builder'
import { DateTime } from 'luxon'

interface PaginationParams {
  limit?: number
  __nonce?: string
  __token?: string
}

interface PaginationResponse {
  results: Array<any>
  next: string | null
  prev: string | null
}

export interface EventsIndexParams extends PaginationParams {
  calender_id: number
  starting?: string
  until?: string
  tags?: string
}

interface AutoResponse {
  broadcaster_id: number | null
  subject: string | null
  body: string | null
}

interface Contact {
  name: string
  phone: string
  show_phone: boolean
  email: string
  show_email: boolean
}

interface RSVPForm {
  phone: string
  address: string
  allow_guests: boolean
  accept_rsvps: boolean
  gather_volunteers: boolean
}

interface Address {
  address1: string
  address2: string | null
  address3: string | null
  city: string
  state: string
  country_code: string
  zip: string
  lat: string
  lng: string
}

interface Venue {
  name: string
  address: Address
}

export interface Event {
  id: number
  slug: string
  path: string
  status: string
  site_slug: string
  name: string
  headline: string
  title: string
  excerpt: string | null
  author_id: number | null
  published_at: string
  external_id: number | null
  tags: Array<string>
  intro: string
  calendar_id: number
  contact: Contact
  start_time: string
  end_time: string
  time_zone: string
  rsvp_form: RSVPForm
  capacity: number
  show_guests: boolean
  venue: Venue
  autoresponse: AutoResponse
}

export interface EventsGetResponse extends PaginationResponse {
  results: Event[]
}

export interface Person {
  created_at: string
  email: string | null
  first_name: string | null
  id: number
  last_name: string | null
  mobile: string | null
  phone: string | null
  tags: string[]
  updated_at: string
}

export interface PeopleSearchResponse extends PaginationResponse {
  results: Person[]
}

export interface PeopleSearchParams extends PaginationParams {
  updated_since?: string | null
}

export default class NationBuilderService {
  readonly baseURL: string
  private readonly client: AxiosInstance
  private readonly siteSlug: string

  constructor() {
    this.siteSlug = nbConfig.siteSlug
    this.baseURL = `https://${nbConfig.nationSlug}.nationbuilder.com`
    this.client = axios.create({
      baseURL: `${this.baseURL}/api/v1/`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      params: {
        access_token: nbConfig.accessToken,
      },
    })
  }

  /**
   * Retrieves a list of volunteer events from NationBuilder.
   */
  async getVolunteerEvents(
    params: Partial<EventsIndexParams> = {},
    next: string | null = null
  ): Promise<EventsGetResponse> {
    params = {
      calender_id: nbConfig.volunteerEventsCalendarId,
      limit: nbConfig.limit,
      starting: DateTime.now().toISO(),
      tags: nbConfig.volunteerEventsFilterSlugs,
      ...params,
    }
    this.applyPaginationParams(params, next)
    const response = await this.client.get(`sites/${this.siteSlug}/pages/events`, { params })
    return <EventsGetResponse>response.data
  }

  /**
   * Search people within NationBuilder.
   */
  async peopleSearch(
    params: Partial<PeopleSearchParams> = {},
    next: string | null = null
  ): Promise<PeopleSearchResponse> {
    params = {
      limit: nbConfig.limit,
      ...params,
    }
    this.applyPaginationParams(params, next)
    const response = await this.client.get('people/search', { params })
    return <PeopleSearchResponse>response.data
  }

  private applyPaginationParams(params: PaginationParams, next: string | null) {
    if (next) {
      const url = new URL(next, this.baseURL)
      params.__nonce = <string>url.searchParams.get('__nonce')
      params.__token = <string>url.searchParams.get('__token')
    }
  }

  normalizeTimezone(zone: string): string {
    switch (zone) {
      case 'Hawaii':
        return 'Pacific/Honolulu'
      case 'Alaska':
        return 'America/Anchorage'
      case 'Pacific Time (US & Canada)':
        return 'America/Los_Angeles'
      case 'Arizona':
        return 'America/Phoenix'
      case 'Mountain Time (US & Canada)':
        return 'America/Denver'
      case 'Central Time (US & Canada)':
        return 'America/Chicago'
      case 'Eastern Time (US & Canada)':
      case 'Indiana (East)':
        return 'America/New_York'
      default:
        return zone
    }
  }

  parseDateTime(dateTimeStr: string, zone: string): DateTime {
    return DateTime.fromISO(dateTimeStr).setZone(this.normalizeTimezone(zone))
  }
}
