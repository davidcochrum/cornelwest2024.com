import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import bolt, { Block, KnownBlock } from '@slack/bolt'
import { WebClient } from '@slack/web-api'
import config from '#config/slack'
import BallotAccessState from '#models/ballot_access_state'
import SlackUtils from '#services/love_bot/slack_utils'
import NationBuilderService, { Event } from '#services/nation_builder_service'
import { DateTime } from 'luxon'
import TurndownService from 'turndown'

const turndownSvc = new TurndownService()
turndownSvc.escape = (str) => str // don't escape any html that may look like markdown

// noinspection JSUnusedGlobalSymbols
@inject()
export default class SlackService {
  private _client: WebClient | null = null

  constructor(
    private logger: Logger,
    private nb: NationBuilderService,
  ) {
  }

  get client() {
    if (!this._client) {
      const app = new bolt.App({
        appToken: config.appToken,
        logLevel: config.logLevel,
        signingSecret: config.signingSecret,
        socketMode: false,
        token: config.token,
      })
      this._client = app.client
      this.logger.debug(this._client, 'Initialized Slack client')
    }
    return this._client
  }

  /**
   * Posts a message in a Slack channel.
   */
  async say(channel: string, text: string, blocks: Array<Block | KnownBlock> = []) {
    return this.client.chat.postMessage({
      channel,
      text,
      blocks,
    })
  }

  /**
   * Posts an announcement message for a NationBuilder event within the appropriate state Slack channel.
   */
  async postNationBuilderEvent(event: Event, header: string) {
    const state = await BallotAccessState.findBy('code', event.venue.address.state)
    if (!state) {
      this.logger.error(`No state configured matching event address: ${event.venue.address.state}`)
      return
    }
    if (!state?.channel) {
      this.logger.warn(`No channel configured for state: ${state.code}`)
      return
    }

    const channel = await SlackUtils.getPrivateChannelId(this.client, state.channel)
    if (!channel) {
      this.logger.error(`Unable to find channel ID for #${state.channel}`)
      return
    }

    const startDT = this.nb.parseDateTime(event.start_time, event.time_zone)
    let location = [
      event.venue.name,
      event.venue.address.address1,
      event.venue.address.address2,
      event.venue.address.city,
      event.venue.address.state,
    ]
      .filter((val) => 0 < String(val).length)
      .join(', ')
    if (event.venue.address.zip) {
      location += ` ${event.venue.address.zip}`
    }
    let locationLink = ''
    const locationSearchCombo = [ event.venue.name, event.venue.address.address1, event.intro ].join(
      ' ',
    )
    const isVirtual = !!/meet\.google|google\s?meet|zoom/gi.exec(locationSearchCombo)
    const virtualLink = /[^\s"<>]*(meet\.google\.com|zoom\.us|zoom\.com)[^\s<>"]*/i.exec(
      locationSearchCombo,
    )
    if (virtualLink) {
      locationLink = virtualLink[0]
    } else if (!isVirtual) {
      locationLink = `https://maps.google.com/maps?q=${encodeURI(location)}`
    }
    await this.say(channel, header, [
      {
        type: 'header',
        text: {
          text: header,
          type: 'plain_text',
        },
      },
      {
        type: 'section',
        text: {
          text: `:calendar: ${startDT.toLocaleString(DateTime.DATETIME_FULL)}`,
          type: 'mrkdwn',
        },
        accessory: {
          type: 'button',
          text: {
            type: 'plain_text',
            text: 'RSVP',
          },
          url: this.nb.baseURL + event.path,
        },
      },
      {
        type: 'section',
        text: {
          text: `:round_pushpin: ${location}`,
          type: 'mrkdwn',
        },
        accessory:
          locationLink.length > 0
            ? {
              type: 'button',
              text: {
                type: 'plain_text',
                text: isVirtual ? 'Link' : 'Map',
              },
              url: locationLink,
            }
            : undefined,
      },
      {
        type: 'section',
        text: {
          text: `:bust_in_silhouette: ${event.contact.name}`,
          type: 'mrkdwn',
        },
        accessory: event.contact.email
          ? {
            type: 'button',
            text: {
              type: 'plain_text',
              text: 'Email',
            },
            url: `mailto:${event.contact.email}`,
          }
          : undefined,
      },
      {
        type: 'section',
        text: {
          text: turndownSvc.turndown(event.intro),
          type: 'mrkdwn',
        },
      },
    ])
  }
}
