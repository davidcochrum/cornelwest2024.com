import { inject } from '@adonisjs/core'
import { Logger } from '@adonisjs/core/logger'
import NBPersonUpdated from '#events/nb_person_updated'
import CallHubService from '#services/callhub_service'

@inject()
export default class UpdateCallHubContact {
  constructor(
    private logger: Logger,
    private ch: CallHubService
  ) {}

  async handle({ model }: NBPersonUpdated) {
    const phone = model.phone || model.mobile
    if (!phone) {
      this.logger.debug('No phone number to match in CallHub')
      return
    }
    if (model.doNotCall && model.doNotText) {
      this.logger.debug('Contact requests no contact so skip')
      return
    }

    const contact = await this.ch.contactFirstOrCreate(model.chId, {
      contact: phone,
      mobile: model.mobile || undefined,
      first_name: model.firstName || undefined,
      last_name: model.lastName || undefined,
      email: model.email || undefined,
    })

    if (!model.chId) {
      model.chId = contact.pk_str
      await model.save()
    }

    await this.ch.contactTag(contact, model.tags)
    this.logger.info({ pk_str: contact.pk_str, tags: model.tags }, 'CallHub contact tagged')
  }
}
