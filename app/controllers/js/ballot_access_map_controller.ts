import { HttpContext } from '@adonisjs/core/http'
import path from 'node:path'
import fs from 'fs'

export default class BallotAccessMapController {
  async handle({ response }: HttpContext) {
    const manifestPath = path.resolve('public', 'assets', '.vite', 'manifest.json')
    const manifest = JSON.parse((await fs.promises.readFile(manifestPath)).toString())
    const manifestFile = manifest['resources/js/ballot_access_map.js']['file']
    response.redirect().toPath(`/assets/${manifestFile}`)
  }
}
