import type { HttpContext } from '@adonisjs/core/http'
import BallotAccessState from '#models/ballot_access_state'
import { updateBallotAccessStateValidator } from '#validators/ballot_access_state'
import { ModelObject } from '@adonisjs/lucid/types/model'

export default class BallotAccessStatesController {
  /**
   * Display a list of resource
   */
  async index({}: HttpContext) {
    const states = await BallotAccessState.all()
    const statesSorted = states.toSorted((a, b) => a.name.localeCompare(b.name))
    let statesKeyed: { [x: string]: ModelObject } = {}
    statesSorted.forEach((state) => (statesKeyed[state.code] = state.serialize()))
    return statesKeyed
  }

  /**
   * Show individual record
   */
  async show({ params }: HttpContext) {
    const state = await BallotAccessState.findOrFail(params.code)
    return state.serialize()
  }

  /**
   * Handle form submission for the edit action
   */
  async update({ params, request }: HttpContext) {
    const state = await BallotAccessState.findOrFail(params.code)
    const payload = await request.validateUsing(updateBallotAccessStateValidator)
    await state.merge(payload).save()
    return state.serialize()
  }
}
