import type { HttpContext } from '@adonisjs/core/http'
import User from '#models/user'

export default class AuthController {
  async login({ request }: HttpContext) {
    const { email, password } = request.only(['email', 'password'])
    const user = await User.verifyCredentials(email, password)

    // clear out any expired tokens
    const oldTokens = await User.accessTokens.all(user)
    for (const oldToken of oldTokens) {
      if (oldToken.isExpired()) {
        await User.accessTokens.delete(user, oldToken.identifier)
      }
    }

    // issue new token
    const token = await User.accessTokens.create(user, ['*'], { name: 'api', expiresIn: '30 days' })
    return { ...user.serialize(), ...{ token: token.toJSON() } }
  }

  async logout({ auth, response }: HttpContext) {
    await User.accessTokens.delete(auth.user!, auth.user!.currentAccessToken.identifier)
    response.status(204)
  }
}
