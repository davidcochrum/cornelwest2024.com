import type { HttpContext } from '@adonisjs/core/http'
import logger from '@adonisjs/core/services/logger'
import NbPersonUpdated from '#events/nb_person_updated'
import Person from '#models/person'

export interface PersonPayload {
  id: number
  do_not_call: boolean
  do_not_contact: boolean
  email: string
  email_opt_in: boolean
  employer: string | null
  federal_donotcall: boolean
  first_name: string
  primary_address: {
    address1: string
    address2: string
    city: string
    state: string
    country_code: string
    zip: string
  }
  last_name: string
  mobile_normalized: string | null
  mobile_opt_in: boolean
  phone_normalized: string | null
  tags: string[]
  title: string
}

export default class NbWebhooksController {
  async person({ request, response }: HttpContext) {
    const payload = request.body().payload.person as PersonPayload
    logger.info(payload, 'NationBuilder person create/update webhook received')

    const doNotCall = payload.do_not_call || payload.do_not_contact || payload.federal_donotcall
    const doNotText = payload.do_not_contact || !payload.mobile_opt_in
    const model = await Person.updateOrCreate(
      { id: payload.id },
      {
        doNotCall: doNotCall,
        doNotText: doNotText,
        email: payload.email,
        firstName: payload.first_name,
        lastName: payload.last_name,
        mobile: payload.mobile_normalized,
        phone: payload.phone_normalized,
        tags: payload.tags,
      }
    )
    await model.refresh()

    await NbPersonUpdated.dispatch(model)

    return response.ok(model.serialize())
  }
}
