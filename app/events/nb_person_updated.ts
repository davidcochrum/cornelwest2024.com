import { BaseEvent } from '@adonisjs/core/events'
import Person from '#models/person'

export default class NbPersonUpdated extends BaseEvent {
  /**
   * Accept event data as constructor parameters
   */
  constructor(readonly model: Person) {
    super()
  }
}
