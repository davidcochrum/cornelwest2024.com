import emitter from '@adonisjs/core/services/emitter'
import NBPersonUpdated from '#events/nb_person_updated'

emitter.on(NBPersonUpdated, [() => import('#listeners/update_call_hub_contact')])
