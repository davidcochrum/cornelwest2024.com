/*
|--------------------------------------------------------------------------
| Environment variables service
|--------------------------------------------------------------------------
|
| The `Env.create` method creates an instance of the Env service. The
| service validates the environment variables and also cast values
| to JavaScript data types.
|
*/

import { Env } from '@adonisjs/core/env'

export default await Env.create(new URL('../', import.meta.url), {
  NODE_ENV: Env.schema.enum(['development', 'production', 'test'] as const),
  PORT: Env.schema.number(),
  APP_KEY: Env.schema.string(),
  HOST: Env.schema.string({ format: 'host' }),
  LOG_LEVEL: Env.schema.string(),

  /*
  |----------------------------------------------------------
  | Variables for configuring the CallHub API
  |----------------------------------------------------------
  */
  CALL_HUB_API_KEY: Env.schema.string(),

  /*
  |----------------------------------------------------------
  | Variables for configuring database connection
  |----------------------------------------------------------
  */
  DATABASE_URL: Env.schema.string(),

  /*
  |----------------------------------------------------------
  | Variables for configuring the NationBuilder API
  |----------------------------------------------------------
  */
  NATION_BUILDER_API_KEY: Env.schema.string(),

  /*
  |----------------------------------------------------------
  | Variables for configuring the Slack Bot
  |----------------------------------------------------------
  */
  SLACK_APP_TOKEN: Env.schema.string(),
  SLACK_SIGNING_SECRET: Env.schema.string(),
  SLACK_BOT_TOKEN: Env.schema.string(),
})
