/*
|--------------------------------------------------------------------------
| Routes file
|--------------------------------------------------------------------------
|
| The routes file is used for defining the HTTP routes.
|
*/

import router from '@adonisjs/core/services/router'
import { middleware } from '#start/kernel'

const AuthController = () => import('#controllers/api/auth_controller')
const BallotAccessMapJsController = () => import('#controllers/js/ballot_access_map_controller')
const BallotAccessStatesController = () =>
  import('#controllers/api/ballot_access_states_controller')
const NBWebhooksController = () => import('#controllers/nb_webhooks_controller')

router.get('/js/ballot-access-map.js', [BallotAccessMapJsController]).as('js.ballot-access-map')
router.on('/petitioning_launch').render('ballot_access_map')
router
  .group(() => {
    router.post('/person', [NBWebhooksController, 'person']).as('person')
  })
  .as('webhooks.nb')
  .prefix('/webhooks/nb')

router
  .group(() => {
    router
      .group(() => {
        router.post('/', [AuthController, 'login']).as('login')
        router.delete('/', [AuthController, 'logout']).use(middleware.auth()).as('logout')
      })
      .as('auth')
      .prefix('/auth')
    router
      .resource('ballot-access-state', BallotAccessStatesController)
      .only(['index', 'show', 'update'])
      .params({ 'ballot-access-state': 'code' })
      .use('update', middleware.auth())
      .as('ballot-access-state')
  })
  .as('api')
  .prefix('/api')

router.on('*').render('spa')
