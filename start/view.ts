import edge from 'edge.js'
import env from '#start/env'

/**
 * Define a global property
 */
edge.global('ballotAccessDataUrl', env.get('BALLOT_ACCESS_DATA_URL', '/api/ballot-access-state'))
