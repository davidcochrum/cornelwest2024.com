import { defineConfig } from 'vite'
import adonisJs from '@adonisjs/vite/client'
import { sentryVitePlugin } from '@sentry/vite-plugin'
import vue from '@vitejs/plugin-vue'

// noinspection JSUnusedGlobalSymbols
export default defineConfig({
  plugins: [
    adonisJs({
      /**
       * Entrypoints of your application. Each entrypoint will
       * result in a separate bundle.
       */
      entrypoints: ['resources/js/vue/main.js', 'resources/js/ballot_access_map.js'],

      /**
       * Paths to watch and reload the browser on file change
       */
      reload: ['resources/views/**/*.edge'],
    }),
    vue(),
    sentryVitePlugin({
      org: 'cornel-west-2024',
      project: 'cornel-west-2024',
    }),
  ],

  build: {
    sourcemap: true,
  },
})
