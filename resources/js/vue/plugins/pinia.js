import { createPinia } from 'pinia'

/**
 * Registers pinia within a Vue app.
 * @param {App<Element>} app
 */
export default function (app) {
  app.use(createPinia())
}
