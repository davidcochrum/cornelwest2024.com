# Cornel West 2024 Web App

This web app serves various purposes for the 2024 Presidential Campaign of Cornel West, including:
- Slack bot assisting in moderating the official campaign Slack.
    [Feel free to request an invitation](https://www.cornelwest2024.com/volunteer).
- Serving the interactive ballot access map embeddable script: 
    ```html
    <div id="ballot-access-map"></div>
    <script src="https://assets.cornelwest2024.com/js/ballot-access-map.js" defer></script>
    ```
- Serving the editing form to update ballot access information

## Frameworks

This codebase leverages [AdonisJS](https://adonisjs.com) for its backend, [Vue](https://vuejs.org) on the frontend,
and [Bolt](https://slack.dev/bolt-js) for the Slack integration.

## Contributing

1. Use the included [docker-compose.yml](/docker-compose.yml) to spin up a Postgres database
2. Copy the [.env.example](/.env.example) file and adjust it to your needs
3. Install dependencies
    ```shell
    yarn install
    ```
4. Start the backend and frontend compiler:
    ```shell
    node ace serve --watch
    ```
5. Start the Love Bot
    ```shell
    node ace run:love-bot
    ```
