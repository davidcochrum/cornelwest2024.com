import env from '#start/env'

export default {
  key: env.get('CALL_HUB_API_KEY'),
}
