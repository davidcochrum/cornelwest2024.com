import { ProfilingIntegration } from '@sentry/profiling-node'
import env from '#start/env'

// noinspection JSUnusedGlobalSymbols
export default {
  dsn: env.get('SENTRY_DSN'), integrations: [
    new ProfilingIntegration(),
  ],
  tracesSampleRate: Number(env.get('SENTRY_TRACES_SAMPLE_RATE') ?? 1.0),
  profilesSampleRate: Number(env.get('SENTRY_PROFILES_SAMPLE_RATE') ?? 1.0),
}
