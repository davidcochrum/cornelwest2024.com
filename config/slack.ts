import bolt from '@slack/bolt'
import env from '#start/env'

// noinspection JSUnusedGlobalSymbols
export default {
  appToken: env.get('SLACK_APP_TOKEN'),
  logLevel: <bolt.LogLevel>env.get('SLACK_LOG_LEVEL', bolt.LogLevel.INFO),
  signingSecret: env.get('SLACK_SIGNING_SECRET'),
  token: env.get('SLACK_BOT_TOKEN'),
}
