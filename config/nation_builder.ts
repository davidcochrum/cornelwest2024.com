import env from '#start/env'

// noinspection JSUnusedGlobalSymbols
export default {
  nationSlug: 'cornelwest',
  accessToken: env.get('NATION_BUILDER_API_KEY'),
  limit: 200,
  siteSlug: 'cornelwest24',
  volunteerEventsCalendarId: 2,
  volunteerEventsFilterSlugs: 'approved',
}
