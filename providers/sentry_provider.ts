import type { ApplicationService } from '@adonisjs/core/types'
import * as Sentry from '@sentry/node'

export default class SentryProvider {
  constructor(protected app: ApplicationService) {}

  /**
   * Register bindings to the container
   */
  register() {
    // @ts-ignore
    const config: Sentry.NodeOptions = this.app.config.get('sentry')
    Sentry.init(config)
  }
}
