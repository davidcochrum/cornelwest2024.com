import { faker } from '@faker-js/faker'
import { test } from '@japa/runner'
import { PersonPayload } from '#controllers/nb_webhooks_controller'
import Person from '#models/person'
import emitter from '@adonisjs/core/services/emitter'
import NbPersonUpdated from '#events/nb_person_updated'

test.group('NationBuilder Webhook Person', async () => {
  test('{label}')
    .with([
      {
        label: 'contactable',
        payloadOverride: {},
        expect: {},
      },
      {
        label: 'do not call',
        payloadOverride: { do_not_call: true },
        expect: { doNotCall: true },
      },
      {
        label: 'do not contact',
        payloadOverride: { do_not_contact: true },
        expect: { doNotCall: true, doNotText: true },
      },
      {
        label: 'federal do not call',
        payloadOverride: { do_not_call: true },
        expect: { doNotCall: true },
      },
      {
        label: 'mobile opt out',
        payloadOverride: { mobile_opt_in: false },
        expect: { doNotText: true },
      },
      {
        label: 'missing mobile',
        payloadOverride: { mobile_normalized: null },
        expect: {},
      },
      {
        label: 'missing phone',
        payloadOverride: { phone_normalized: null },
        expect: {},
      },
      {
        label: 'missing phone',
        payloadOverride: { phone_normalized: null },
        expect: {},
      },
      {
        label: 'missing mobile and phone',
        payloadOverride: { mobile_normalized: null, phone_normalized: null },
        expect: {},
      },
    ])
    .run(async ({ assert, cleanup, client, route }, { payloadOverride, expect }) => {
      const events = emitter.fake()
      cleanup(() => {
        emitter.restore()
      })

      const payload = {
        id: faker.number.int({ min: 1, max: 99999 }),
        do_not_call: false,
        do_not_contact: false,
        email: faker.internet.email(),
        federal_donotcall: false,
        first_name: faker.person.firstName(),
        last_name: faker.person.lastName(),
        mobile_normalized: faker.string.numeric({ length: 10 }),
        mobile_opt_in: true,
        phone_normalized: faker.string.numeric({ length: 10 }),
        tags: ['tag1', 'tag2'],
        ...payloadOverride,
      } as PersonPayload

      if (faker.datatype.boolean()) {
        // sometimes have an existing record
        await Person.create({
          id: payload.id,
          email: faker.internet.email(),
          firstName: faker.person.firstName(),
          lastName: faker.person.lastName(),
          doNotCall: faker.datatype.boolean(),
          doNotText: faker.datatype.boolean(),
          mobile: faker.string.numeric({ length: 10 }),
          phone: faker.string.numeric({ length: 10 }),
          tags: ['replace me'],
        })
      }

      const resp = await client
        .post(route('webhooks.nb.person'))
        .json({ payload: { person: payload } })

      resp.assertStatus(200)
      const model = await Person.findByOrFail('id', payload.id)
      assert.equal(model.id, payload.id)
      assert.equal(model.chId, null)
      assert.equal(model.email, payload.email)
      assert.equal(model.firstName, payload.first_name)
      assert.equal(model.lastName, payload.last_name)
      assert.equal(model.doNotCall, expect.doNotCall ?? false)
      assert.equal(model.doNotText, expect.doNotText ?? false)
      assert.equal(model.phone, payload.phone_normalized)
      assert.equal(model.mobile, payload.mobile_normalized)
      assert.lengthOf(model.tags, payload.tags.length)
      payload.tags.forEach((tag) => assert.include(model.tags, tag))
      resp.assertBody(model.serialize())

      events.assertEmitted(NbPersonUpdated)
    })
})
