import ace from '@adonisjs/core/services/ace'
import hash from '@adonisjs/core/services/hash'
import { faker } from '@faker-js/faker'
import { test } from '@japa/runner'
import CreateUser from '#commands/create_user'
import User from '#models/user'
import { UserFactory } from '#database/factories/user'

test.group('Commands create user', () => {
  test('should create a user with a generated password when empty', async ({ assert }) => {
    const cmd = await ace.create(CreateUser, [])

    const fullName = faker.person.fullName()
    cmd.prompt.trap('Enter full name').replyWith(fullName)

    const email = faker.internet.email()
    cmd.prompt.trap('Enter email').replyWith(email)

    cmd.prompt.trap('Enter a password (leave blank to generate one)').replyWith('')

    await cmd.exec()

    cmd.assertSucceeded()

    const user = await User.findByOrFail('email', email)
    assert.isAbove(user.id, 0)
    assert.equal(user.fullName, fullName)
    assert.equal(user.email, email)
    assert.notEmpty(user.password)
  })

  test('should create a user with and encrypt the given password', async ({ assert }) => {
    const existingUser = await UserFactory.create()
    const cmd = await ace.create(CreateUser, [])

    const fullName = faker.person.fullName()
    cmd.prompt.trap('Enter full name')
      .assertFails('').assertFails('a')
      .assertPasses('ab')
      .replyWith(fullName)

    const email = faker.internet.email()
    cmd.prompt.trap('Enter email')
      .assertFails('').assertFails('a@').assertFails('@b').assertFails(existingUser.email)
      .assertPasses('a@b')
      .replyWith(email)

    const password = faker.internet.password()
    cmd.prompt.trap('Enter a password (leave blank to generate one)')
      .assertFails('a').assertFails('abc1234')
      .assertPasses('').assertPasses('abcd1234')
      .replyWith(password)

    await cmd.exec()

    cmd.assertSucceeded()

    const user = await User.findByOrFail('email', email)
    assert.isAbove(user.id, 0)
    assert.equal(user.fullName, fullName)
    assert.equal(user.email, email)
    assert.notEmpty(user.password)
    assert.isTrue(await hash.verify(user.password, password))
  })
})
