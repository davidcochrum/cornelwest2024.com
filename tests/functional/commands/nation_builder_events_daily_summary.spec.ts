import ace from '@adonisjs/core/services/ace'
import app from '@adonisjs/core/services/app'
import { test } from '@japa/runner'
import NationBuilderEventsDailySummary from '#commands/nation_builder_events_daily_summary'
import NationBuilderService, { Event, EventsGetResponse } from '#services/nation_builder_service'
import SlackService from '#services/slack_service'
import { Settings } from 'luxon'
import sinon from 'sinon'

test.group('nation builder daily events summary', () => {
  test('do nothing outside 9am for US timezones - {label}')
    .with([
      { label: '9AM UTC', hourUTC: 9 },
      { label: '10AM Honolulu', hourUTC: 20 },
      { label: '8AM Eastern', hourUTC: 12 },
    ])
    .run(async ({}, { hourUTC }) => {
      Settings.now = () => new Date('04-01-01').setUTCHours(hourUTC).valueOf()

      const nb = await app.container.make(NationBuilderService)
      nb.getVolunteerEvents = sinon.stub().throws()
      app.container.swap(NationBuilderService, () => nb)

      const slack = await app.container.make(SlackService)
      slack.postNationBuilderEvent = sinon.stub().throws()
      app.container.swap(SlackService, () => slack)

      const cmd = await ace.create(NationBuilderEventsDailySummary, [])
      await cmd.exec()

      cmd.assertSucceeded()
    })

  test(`post events during the 9AM hour - {label}`)
    .with([
      {
        label: '09:00:00 Eastern',
        hours: 9 + 4,
        min: 0,
        sec: 0,
        starting: '2001-04-01T09:00:00.000-04:00',
        until: '2001-04-02T08:59:59.000-04:00',
        expectEventIds: [ 234, 565 ],
      },
      {
        label: '09:59:59 Eastern',
        hours: 9 + 4,
        min: 59,
        sec: 59,
        starting: '2001-04-01T09:00:00.000-04:00',
        until: '2001-04-02T08:59:59.000-04:00',
        expectEventIds: [ 234, 565 ],
      },
      {
        label: '09:00:00 Central',
        hours: 9 + 5,
        min: 0,
        sec: 0,
        starting: '2001-04-01T09:00:00.000-05:00',
        until: '2001-04-02T08:59:59.000-05:00',
        expectEventIds: [ 123, 654, 698 ],
      },
      {
        label: '09:59:59 Central',
        hours: 9 + 5,
        min: 59,
        sec: 59,
        starting: '2001-04-01T09:00:00.000-05:00',
        until: '2001-04-02T08:59:59.000-05:00',
        expectEventIds: [ 123, 654, 698 ],
      },
      {
        label: '09:00:00 Mountain',
        hours: 9 + 6,
        min: 0,
        sec: 0,
        starting: '2001-04-01T09:00:00.000-06:00',
        until: '2001-04-02T08:59:59.000-06:00',
        expectEventIds: [ 825 ],
      },
      {
        label: '09:59:59 Mountain',
        hours: 9 + 6,
        min: 59,
        sec: 59,
        starting: '2001-04-01T09:00:00.000-06:00',
        until: '2001-04-02T08:59:59.000-06:00',
        expectEventIds: [ 825 ],
      },
      {
        label: '09:00:00 Pacific',
        hours: 9 + 7,
        min: 0,
        sec: 0,
        starting: '2001-04-01T09:00:00.000-07:00',
        until: '2001-04-02T08:59:59.000-07:00',
        expectEventIds: [ 621, 324, 741 ],
      },
      {
        label: '09:59:59 Pacific',
        hours: 9 + 7,
        min: 59,
        sec: 59,
        starting: '2001-04-01T09:00:00.000-07:00',
        until: '2001-04-02T08:59:59.000-07:00',
        expectEventIds: [ 621, 324, 741 ],
      },
      {
        label: '09:00:00 Alaska',
        hours: 9 + 8,
        min: 0,
        sec: 0,
        starting: '2001-04-01T09:00:00.000-08:00',
        until: '2001-04-02T08:59:59.000-08:00',
        expectEventIds: [ 145 ],
      },
      {
        label: '09:59:59 Alaska',
        hours: 9 + 8,
        min: 59,
        sec: 59,
        starting: '2001-04-01T09:00:00.000-08:00',
        until: '2001-04-02T08:59:59.000-08:00',
        expectEventIds: [ 145 ],
      },
      {
        label: '09:00:00 Hawaii',
        hours: 9 + 10,
        min: 0,
        sec: 0,
        starting: '2001-04-01T09:00:00.000-10:00',
        until: '2001-04-02T08:59:59.000-10:00',
        expectEventIds: [ 374 ],
      },
      {
        label: '09:59:59 Hawaii',
        hours: 9 + 10,
        min: 59,
        sec: 59,
        starting: '2001-04-01T09:00:00.000-10:00',
        until: '2001-04-02T08:59:59.000-10:00',
        expectEventIds: [ 374 ],
      },
    ])
    .run(async ({}, { hours, min, sec, starting, until, expectEventIds }) => {
      Settings.now = () => new Date('04-01-01').setUTCHours(hours, min, sec).valueOf()

      const nb = await app.container.make(NationBuilderService)
      const getVolunteerEventsStub = sinon.stub().resolves({
        results: [
          {
            id: 123,
            headline: 'Texas event',
            start_time: '2024-04-01T18:00:00-05:00',
            time_zone: 'Central Time (US & Canada)',
          } as Event,
          {
            id: 234,
            headline: 'Florida event',
            start_time: '2024-04-01T17:30:00-04:00',
            time_zone: 'Eastern Time (US & Canada)',
          } as Event,
          {
            id: 145,
            headline: 'Alaska event',
            start_time: '2024-04-01T16:17:00-08:00',
            time_zone: 'Alaska',
          } as Event,
          {
            id: 565,
            headline: 'Indiana event',
            start_time: '2024-04-01T19:45:00-04:00',
            time_zone: 'Indiana (East)',
          } as Event,
          {
            id: 654,
            headline: 'Chicago event',
            start_time: '2024-04-01T11:35:00-05:00',
            time_zone: 'Central Time (US & Canada)',
          } as Event,
          {
            id: 621,
            headline: 'Arizona event',
            start_time: '2024-04-01T22:00:00-07:00',
            time_zone: 'Arizona',
          } as Event,
          {
            id: 324,
            headline: 'California event',
            start_time: '2024-04-01T19:45:00-07:00',
            time_zone: 'Pacific Time (US & Canada)',
          } as Event,
          {
            id: 374,
            headline: 'Hawaii event',
            start_time: '2024-04-01T13:15:00-10:00',
            time_zone: 'Hawaii',
          } as Event,
          {
            id: 825,
            headline: 'Colorado event',
            start_time: '2024-04-01T14:35:00-06:00',
            time_zone: 'Mountain Time (US & Canada)',
          } as Event,
          {
            id: 741,
            headline: 'Nevada event',
            start_time: '2024-04-01T19:45:00-07:00',
            time_zone: 'Pacific Time (US & Canada)',
          } as Event,
          {
            id: 698,
            headline: 'Kansas event',
            start_time: '2024-04-01T09:30:00-05:00',
            time_zone: 'Central Time (US & Canada)',
          } as Event,
        ],
        next: null,
        prev: null,
      } as EventsGetResponse)
      nb.getVolunteerEvents = getVolunteerEventsStub
      app.container.swap(NationBuilderService, () => nb)

      const slack = await app.container.make(SlackService)
      const postNBEventStub = sinon.stub().resolves()
      slack.postNationBuilderEvent = postNBEventStub
      app.container.swap(SlackService, () => slack)

      const cmd = await ace.create(NationBuilderEventsDailySummary, [])
      await cmd.exec()

      cmd.assertSucceeded()
      sinon.assert.calledWith(getVolunteerEventsStub, { starting, until })
      sinon.assert.callCount(postNBEventStub, expectEventIds.length)
      for (const expectEventId of expectEventIds) {
        sinon.assert.calledWithMatch(postNBEventStub, { id: expectEventId })
      }
    })
})
