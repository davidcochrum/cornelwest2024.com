import ace from '@adonisjs/core/services/ace'
import hash from '@adonisjs/core/services/hash'
import { faker } from '@faker-js/faker'
import { test } from '@japa/runner'
import PasswordReset from '#commands/password_reset'
import { UserFactory } from '#database/factories/user'

test.group('Commands reset password', () => {
  test('should update the password for a user as provided', async ({ assert }) => {
    const oldPassword = faker.internet.password()
    const user = await UserFactory.merge({ password: oldPassword }).create()
    const cmd = await ace.create(PasswordReset, [])

    cmd.prompt.trap('Enter email')
      .assertFails('').assertFails(faker.internet.email())
      .assertPasses(user.email)
      .replyWith(user.email)

    const newPassword = faker.internet.password()
    cmd.prompt.trap('Enter a password (leave blank to generate one)')
      .assertFails('a').assertFails('abc1234')
      .assertPasses('').assertPasses('abcd1234')
      .replyWith(newPassword)

    await cmd.exec()

    cmd.assertSucceeded()

    await user.refresh()
    assert.isAbove(user.password.length, 64)
    await hash.assertNotEquals(user.password, oldPassword)
  })
  test('should generate a password for a user when not provided', async ({ assert }) => {
    const oldPass = 'old-pass'
    const user = await UserFactory.merge({ password: oldPass }).create()
    const cmd = await ace.create(PasswordReset, [])

    cmd.prompt.trap('Enter email').replyWith(user.email)
    cmd.prompt.trap('Enter a password (leave blank to generate one)').assertPasses(oldPass).replyWith('')

    await cmd.exec()

    cmd.assertSucceeded()

    await user.refresh()
    assert.notEmpty(user.password)
    await hash.assertNotEquals(user.password, oldPass)
  })
})
