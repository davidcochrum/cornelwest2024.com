import { test } from '@japa/runner'
import fs from 'fs'
import path from 'node:path'

test.group('Ballot access map JS redirect', (group) => {
  const assetsDir = path.join('public', 'assets', '.vite')
  const manifestPath = path.join(assetsDir, 'manifest.json')
  const manifestPathBak = `${manifestPath}.bak`
  group.setup(async () => {
    if (fs.existsSync(manifestPath)) {
      await fs.promises.rename(manifestPath, manifestPathBak)
    } else {
      await fs.promises.mkdir(assetsDir, { recursive: true })
    }
  })
  group.teardown(async () => {
    if (fs.existsSync(manifestPathBak)) {
      if (fs.existsSync(manifestPath)) {
        await fs.promises.unlink(manifestPath)
      }
      fs.renameSync(manifestPathBak, manifestPath)
    }
  })

  test('redirects to the built ballot access map JS', async ({ client, route }) => {
    await fs.promises.writeFile(manifestPath, JSON.stringify({
      'resources/js/ballot_access_map.js': {
        'file': 'ballot_access_map-DnlrtKHZ.js',
        'src': 'resources/js/ballot_access_map.js',
        'isEntry': true,
        'imports': [
          '__sentry-release-injection-file-B5PT8DtJ.js',
        ],
      },
    }))

    const resp = await client.get(route('js.ballot-access-map'))

    resp.assertRedirectsTo('/assets/ballot_access_map-DnlrtKHZ.js')
  })
})
