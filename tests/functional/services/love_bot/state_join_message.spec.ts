import { Logger } from '@adonisjs/core/logger'
import { test } from '@japa/runner'
import sinon from 'sinon'
import StateJoinMessage from '#services/love_bot/message/state_join'
import BallotAccessState from '#models/ballot_access_state'
import { faker } from '@faker-js/faker'
import { DateTime } from 'luxon'

test.group('bot state join message', async (group) => {
  let logger: Logger
  let handler: StateJoinMessage
  group.each.setup(async () => {
    logger = sinon.createStubInstance(Logger)
    handler = new StateJoinMessage(logger)
    await BallotAccessState.truncate()
    await BallotAccessState.updateOrCreate({ code: 'CA' }, {
      name: 'California',
      channel: '#california',
      matchWords: [ 'ca', 'cali', 'california' ],
    })
    await BallotAccessState.updateOrCreate({ code: 'FL' }, {
      name: 'Florida',
      channel: null,
      matchWords: [ 'fl', 'florida' ],
    })
    await BallotAccessState.updateOrCreate({ code: 'OR' }, {
      name: 'Oregon',
      channel: '#oregon',
      matchWords: [ 'oregon' ],
    })
    await BallotAccessState.updateOrCreate({ code: 'TX' }, {
      name: 'Texas',
      channel: '#texas',
      matchWords: [ 'tx', 'texas', 'tejas' ],
    })
  })

  test(`ignores messages - {label}`)
    .with([
      { label: 'containing zoom', message: 'Join me on the California Zoom call' },
      { label: 'not containing a match word', message: 'I want to be added to Californication' },
      { label: 'containing state code not included as match word', message: 'This OR that' },
      { label: 'when state missing channel', message: 'I would like an invite to Florida' },
    ])
    .run(async ({}, { message }) => {
      const client = sinon.mock({})
      const say = sinon.fake()

      await handler.joinRequestHandler({ client, message: { text: message }, say })

      sinon.assert.notCalled(say)
    })

  test(`ignores messages when user already in channel`, async () => {
    const list = sinon.fake.resolves({
      channels: [
        { name: '#california', id: 'C123' },
        { name: '#utah', id: 'C456' },
      ],
      response_metadata: { next_cursor: '' },
    })
    const members = sinon.fake.resolves({
      members: [ 'U098', 'U765', 'U234' ],
    })
    const client = { conversations: { list, members } }
    const say = sinon.fake()

    await handler.joinRequestHandler({ client, message: { text: 'please add me to CA', user: 'U765' }, say })

    sinon.assert.calledOnce(list)
    sinon.assert.calledWith(list, { types: 'private_channel' })
    sinon.assert.calledOnce(members)
    sinon.assert.calledWith(members, { channel: 'C123' })
    sinon.assert.notCalled(say)
  })

  test(`responds to messages containing a match word when not in channel - {matchWord}`)
    .with([
      { matchWord: 'CALIFORNIA', text: 'CAN I JOIN THE CALIFORNIA CHANNEL?', stateName: 'California', channelId: 'C123' },
      { matchWord: 'Cali', text: 'I want to be added to Cali', stateName: 'California', channelId: 'C123' },
      { matchWord: 'ca', text: 'i would like an invite to ca', stateName: 'California', channelId: 'C123' },
      { matchWord: 'oregon', text: 'i would like an invite to oregon', stateName: 'Oregon', channelId: 'C567' },
      { matchWord: 'tejas', text: 'puedo unirme tejas?', stateName: 'Texas', channelId: 'C987' },
    ])
    .run(async ({}, { text, stateName, channelId }) => {
      const list = sinon.fake.resolves({
      channels: [
        { name: '#california', id: 'C123' },
        { name: '#oregon', id: 'C567' },
        { name: '#texas', id: 'C987' },
        { name: '#utah', id: 'C456' },
      ],
      response_metadata: { next_cursor: '' },
    })
      const members = sinon.fake.resolves({
        members: [ 'U098', 'U765', 'U234' ],
      })
      const client = { conversations: { list, members } }
      const say = sinon.fake.resolves(null)
      const message = {
        text,
        ts: DateTime.fromJSDate(faker.date.anytime()).toMillis(),
        user: 'U123',
      }

      await handler.joinRequestHandler({ client, message, say })

      sinon.assert.calledOnceWithMatch(members, { channel: channelId })
      sinon.assert.calledOnceWithMatch(say, {
        response_type: 'in_channel',
        replace_original: false,
        text: `Hey there <@U123>, did I understand correctly that you wish to join the ${stateName} channel?`,
        thread_ts: message.ts,
      })
    })
})
