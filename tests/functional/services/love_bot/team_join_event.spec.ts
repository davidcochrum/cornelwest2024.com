import { Logger } from '@adonisjs/core/logger'
import { test } from '@japa/runner'
import bolt from '@slack/bolt'
import { WebClient } from '@slack/web-api'
import sinon from 'sinon'
import TeamJoinEvent from '#services/love_bot/event/team_join'

test('team join event', async () => {
  const logger = sinon.createStubInstance(Logger)
  const postMessage = sinon.fake()
  // @ts-ignore
  const client = {
    chat: {
      postMessage,
    },
  } as Partial<WebClient>
  const event = {
    type: 'team_join',
    user: {
      id: 'U1238723',
    },
  } as bolt.TeamJoinEvent

  const handler = new TeamJoinEvent(logger)
  // @ts-ignore
  await handler.handle({ client, event })

  sinon.assert.calledOnceWithMatch(postMessage, {
    channel: 'U1238723',
    text: `Welcome to the Cornel West 2024 Slack Workspace! We're so happy you've joined us.

1. You can access this Slack workspace either on a desktop or mobile app (download is free).
2. If you haven't joined SocialRoots yet, please join the main group here and join your respective subgroups:
   https://socialroots.wicked.coop/invitation?g=db79a3f48f0511eeb3257e47a2f35955&k=5cf6c6.
   You'll want to join to receive regular updates from your state's quartet of leaders and the campaign as well.
3. Slack has an extensive amount of tutorials for users of all experience levels here:
   https://slack.com/help/categories/360000049063#tutorials
   and you can join the SocialRoots Help Desk subgroup for guides on that platform.
4. Getting started with this campaign is easy - check out this Getting Started Guide to find how you want to help!
   You can take up a leadership role in your state, you can host activities, you can petition for Dr. West in your
   state to help him appear on the general election ballot in your state (he won't be on primary ballots, as he's
   running as an independent) - but overall, we need you to contribute who you are to the campaign - and this movement
   that will go beyond! One pager here:
   https://docs.google.com/document/d/1TpCYv3vfULv6j4BxBx0RyDgYM20Ypb665wcAN37Y38w/edit?usp=sharing`,
    mrkdwn: true,
  })
})
