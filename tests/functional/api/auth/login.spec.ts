import { test } from '@japa/runner'
import { faker } from '@faker-js/faker'
import { UserFactory } from '#database/factories/user'

test.group('Auth login', async () => {
  test('successfully logs in and issues token', async ({ assert, client, route }) => {
    const password = faker.internet.password()
    const user = await UserFactory.merge({ password }).create()
    const email = user.email

    const resp = await client.post(route('api.auth.login'))
      .json({ email, password })
      .loginAs(user)

    resp.assertStatus(200)
    resp.assertBodyContains({
      id: user.id,
      email,
      fullName: user.fullName,
      token: {
        abilities: ['*'],
        name: 'api',
        type: 'bearer',
      },
    })
    resp.assertBodyNotContains({ password })
    assert.match(resp.body().token?.token, /^oat_[a-z]{2}\.[a-z0-9]{64,}$/i)
  })
})
