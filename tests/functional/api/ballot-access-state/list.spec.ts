import { test } from '@japa/runner'
import BallotAccessState, { BallotAccessStatus } from '#models/ballot_access_state'
import { DateTime } from 'luxon'

test.group('Ballot access state list', () => {
  test('get a list of ballot access states data', async ({ client, route }) => {
    await BallotAccessState.updateOrCreateMany('code', [
      {
        code: 'AK',
        name: 'Alaska',
        channel: 'alaska',
        matchWords: [],
        signaturesRequired: null,
        startDate: null,
        deadlineDate: null,
        note: 'On Ballot Already',
        campaignLink: 'https://www.cornelwest2024.com/alaska',
        status: BallotAccessStatus.OnBallot,
      },
      {
        code: 'DC',
        name: 'Washington D.C.',
        channel: 'district-of-columbia',
        matchWords: [ 'dc', 'waaaaa' ],
        signaturesRequired: 4465,
        startDate: DateTime.fromISO('2024-06-14'),
        deadlineDate: DateTime.fromISO('2024-08-07'),
        note: null,
        status: BallotAccessStatus.NotOpen,
      },
      {
        code: 'FL',
        name: 'Florida',
        channel: 'florida',
        matchWords: [ 'fl', 'florida', 'flowers', 'fla', 'floriduh' ],
        signaturesRequired: 145040,
        startDate: null,
        deadlineDate: DateTime.fromISO('2024-07-15'),
        note: 'Need Electors and VP',
        status: BallotAccessStatus.NeedElectors,
      },
      {
        code: 'HI',
        name: 'Hawaii',
        channel: 'hawaii',
        signaturesRequired: 862,
        startDate: DateTime.fromISO('2024-01-01'),
        deadlineDate: DateTime.fromISO('2024-02-22'),
        note: 'Forming New Party',
        campaignLink: 'https://www.cornelwest2024.com/hawaii_for_west',
        stateLink: 'https://elections.hawaii.gov/voting/political-parties/',
        status: BallotAccessStatus.InProgress,
      },
    ])

    const resp = await client.get(route('api.ballot_access_state.index'))

    resp.assertStatus(200)
    resp.assertBodyContains({
      AK: {
        code: 'AK',
        name: 'Alaska',
        channel: 'alaska',
        matchWords: [],
        signaturesRequired: null,
        startDate: null,
        deadlineDate: null,
        note: 'On Ballot Already',
        campaignLink: 'https://www.cornelwest2024.com/alaska',
        status: BallotAccessStatus.OnBallot,
      },
      DC: {
        code: 'DC',
        name: 'Washington D.C.',
        channel: 'district-of-columbia',
        matchWords: [ 'dc', 'waaaaa' ],
        signaturesRequired: 4465,
        startDate: '2024-06-14',
        deadlineDate: '2024-08-07',
        note: null,
        status: BallotAccessStatus.NotOpen,
      },
      FL: {
        code: 'FL',
        name: 'Florida',
        channel: 'florida',
        matchWords: [ 'fl', 'florida', 'flowers', 'fla', 'floriduh' ],
        signaturesRequired: 145040,
        startDate: null,
        deadlineDate: '2024-07-15',
        note: 'Need Electors and VP',
        status: BallotAccessStatus.NeedElectors,
      },
      HI: {
        code: 'HI',
        name: 'Hawaii',
        channel: 'hawaii',
        matchWords: [],
        signaturesRequired: 862,
        startDate: '2024-01-01',
        deadlineDate: '2024-02-22',
        note: 'Forming New Party',
        campaignLink: 'https://www.cornelwest2024.com/hawaii_for_west',
        stateLink: 'https://elections.hawaii.gov/voting/political-parties/',
        status: BallotAccessStatus.InProgress,
      },
    })
  })
})
