import { test } from '@japa/runner'
import { DateTime } from 'luxon'
import { UserFactory } from '#database/factories/user'
import BallotAccessState, { BallotAccessStatus } from '#models/ballot_access_state'
import User from '#models/user'

test.group('Ballot access state edit', async (group) => {
  let user: User
  group.each.setup(async () => {
    user = await UserFactory.create()
    await BallotAccessState.updateOrCreate({ code: 'TX' }, {
      name: 'Texas',
      channel: 'texas',
      matchWords: [ 'texas', 'tex', 'texas', 'tejas' ],
      signaturesRequired: 437,
      startDate: DateTime.fromISO('2024-01-01'),
      deadlineDate: null,
      note: 'someone please update me',
      campaignLink: null,
      stateLink: 'https://texas.gov/third-party',
      status: BallotAccessStatus.NotOpen,
    })
  })

  test('partially updates ballot access data for a state', async ({ assert, client, route }) => {
    const resp = await client.patch(route('api.ballot_access_state.update', { code: 'TX' }))
      .json({
        channel: 'tejas',
        signaturesRequired: 987,
        status: BallotAccessStatus.InProgress,
      })
      .loginAs(user)

    resp.assertStatus(200)
    resp.assertBodyContains({
      code: 'TX',
      name: 'Texas',
      channel: 'tejas',
      matchWords: [ 'texas', 'tex', 'texas', 'tejas' ],
      signaturesRequired: 987,
      startDate: '2024-01-01',
      deadlineDate: null,
      note: 'someone please update me',
      campaignLink: null,
      stateLink: 'https://texas.gov/third-party',
      status: BallotAccessStatus.InProgress,
    })

    const state = await BallotAccessState.findByOrFail('code', 'TX')
    assert.equal('TX', state.code)
    assert.equal('Texas', state.name)
    assert.equal('tejas', state.channel)
    assert.equal(987, state.signaturesRequired)
    assert.equal('2024-01-01', state.startDate?.toISODate())
    assert.equal(null, state.deadlineDate)
    assert.equal('someone please update me', state.note)
    assert.equal(null, state.campaignLink)
    assert.equal('https://texas.gov/third-party', state.stateLink)
    assert.equal(BallotAccessStatus.InProgress, state.status)
  })

  test('fully replaces ballot access data for a state', async ({ assert, client, route }) => {
    const resp = await client.put(route('api.ballot_access_state.update', { code: 'TX' }))
      .json({
        channel: 'tejas',
        signaturesRequired: 987,
        matchWords: [ 'tejas' ],
        startDate: '2024-02-03',
        deadlineDate: '2024-05-07',
        note: 'i updated you',
        campaignLink: 'https://cornelwest2024.com/tejas',
        stateLink: null,
        status: BallotAccessStatus.OnBallot,
      })
      .loginAs(user)

    resp.assertStatus(200)
    resp.assertBodyContains({
      code: 'TX',
      name: 'Texas',
      channel: 'tejas',
      matchWords: [ 'tejas' ],
      signaturesRequired: 987,
      startDate: '2024-02-03',
      deadlineDate: '2024-05-07',
      note: 'i updated you',
      campaignLink: 'https://cornelwest2024.com/tejas',
      stateLink: null,
      status: BallotAccessStatus.OnBallot,
    })

    const state = await BallotAccessState.findByOrFail('code', 'TX')
    assert.equal('TX', state.code)
    assert.equal('Texas', state.name)
    assert.equal('tejas', state.channel)
    assert.equal(987, state.signaturesRequired)
    assert.equal('2024-02-03', state.startDate?.toISODate())
    assert.equal('2024-05-07', state.deadlineDate?.toISODate())
    assert.equal('i updated you', state.note)
    assert.equal('https://cornelwest2024.com/tejas', state.campaignLink)
    assert.equal(null, state.stateLink)
    assert.equal(BallotAccessStatus.OnBallot, state.status)
  })
})
