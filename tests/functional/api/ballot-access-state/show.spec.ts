import { test } from '@japa/runner'
import BallotAccessState, { BallotAccessStatus } from '#models/ballot_access_state'
import { DateTime } from 'luxon'

test.group('Ballot access state show', () => {
  test('show ballot access state data', async ({ client, route }) => {
    await BallotAccessState.updateOrCreateMany('code', [
      {
        code: 'FL',
        name: 'Florida',
        channel: 'florida',
        matchWords: [ 'florida', 'floriduh' ],
        signaturesRequired: 145040,
        startDate: null,
        deadlineDate: DateTime.fromISO('2024-07-15'),
        note: 'Need Electors and VP',
        status: BallotAccessStatus.NeedElectors,
      },
      {
        code: 'HI',
        name: 'Hawaii',
        channel: 'hawaii',
        matchWords: [ 'hawaii', 'mahalo' ],
        signaturesRequired: 862,
        startDate: DateTime.fromISO('2024-01-01'),
        deadlineDate: DateTime.fromISO('2024-02-22'),
        note: 'Forming New Party',
        campaignLink: 'https://www.cornelwest2024.com/hawaii_for_west',
        stateLink: 'https://elections.hawaii.gov/voting/political-parties/',
        status: BallotAccessStatus.InProgress,
      },
    ])

    const resp = await client.get(route('api.ballot_access_state.show', { code: 'HI' }))

    resp.assertStatus(200)
    resp.assertBodyContains({
        code: 'HI',
        name: 'Hawaii',
        channel: 'hawaii',
        matchWords: [ 'hawaii', 'mahalo' ],
        signaturesRequired: 862,
        startDate: '2024-01-01',
        deadlineDate: '2024-02-22',
        note: 'Forming New Party',
        campaignLink: 'https://www.cornelwest2024.com/hawaii_for_west',
        stateLink: 'https://elections.hawaii.gov/voting/political-parties/',
        status: BallotAccessStatus.InProgress,
      },
    )
  })
})
