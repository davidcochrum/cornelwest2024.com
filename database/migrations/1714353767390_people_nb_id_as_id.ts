import { BaseSchema } from '@adonisjs/lucid/schema'

// noinspection JSUnusedGlobalSymbols
export default class extends BaseSchema {
  protected tableName = 'people'

  async up() {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('id')
      table.renameColumn('nb_id', 'id')
      table.primary(['id'])
    })
  }

  async down() {
    this.schema.table(this.tableName, (table) => {
      table.renameColumn('id', 'nb_id')
      table.bigIncrements('id').primary()
    })
  }
}
