import { BaseSchema } from '@adonisjs/lucid/schema'

// noinspection JSUnusedGlobalSymbols
export default class extends BaseSchema {
  protected tableName = 'ballot_access_states'

  async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.json('match_words').defaultTo('[]').after('name')
    })
  }

  async down() {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('match_words')
    })
  }
}
