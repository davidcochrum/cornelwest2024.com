import { BaseSchema } from '@adonisjs/lucid/schema'
import { BallotAccessStatus } from '#models/ballot_access_state'

export default class extends BaseSchema {
  protected tableName = 'ballot_access_states'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.string('code').primary()
      table.string('name')
      table.string('channel').nullable().defaultTo(null)
      table.integer('signatures_required').nullable().defaultTo(null)
      table.date('start_date').nullable().defaultTo(null)
      table.date('deadline_date').nullable().defaultTo(null)
      table.string('note').nullable().defaultTo(null)
      table.string('campaign_link').nullable().defaultTo(null)
      table.string('state_link').nullable().defaultTo(null)
      table.enum('status', Object.values(BallotAccessStatus))
      table.timestamps()
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
