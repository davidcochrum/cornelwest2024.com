import { BaseSchema } from '@adonisjs/lucid/schema'

// noinspection JSUnusedGlobalSymbols
export default class extends BaseSchema {
  protected tableName = 'processed_volunteer_events'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('id')
      table.timestamp('at')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
