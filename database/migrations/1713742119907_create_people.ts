import { BaseSchema } from '@adonisjs/lucid/schema'

// noinspection JSUnusedGlobalSymbols
export default class extends BaseSchema {
  protected tableName = 'people'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.bigIncrements('id').primary()
      table.bigint('nb_id').comment('Person ID within NationBuilder').unique()
      table.bigint('ch_id').comment('Contact ID within CallHub').unique()
      table.string('email')
      table.string('first_name')
      table.string('last_name')
      table.boolean('do_not_call')
      table.boolean('do_not_text')
      table.string('mobile')
      table.string('phone')
      table.json('tags')
      table.timestamps({ defaultToNow: true })
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
