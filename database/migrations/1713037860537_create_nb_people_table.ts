import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'nb_people'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.bigint('id').primary()
      table.string('email')
      table.string('first_name')
      table.string('last_name')
      table.string('phone')
      table.json('tags')
      table.timestamp('created_at')
      table.timestamp('updated_at').index()
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
