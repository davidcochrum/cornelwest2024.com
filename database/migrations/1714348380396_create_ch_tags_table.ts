import { BaseSchema } from '@adonisjs/lucid/schema'

// noinspection JSUnusedGlobalSymbols
export default class extends BaseSchema {
  protected tableName = 'ch_tags'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.string('pk_str').primary()
      table.string('name').notNullable()
      table.timestamp('created_at')
      table.timestamp('updated_at')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
