import { BaseSchema } from '@adonisjs/lucid/schema'

// noinspection JSUnusedGlobalSymbols
export default class extends BaseSchema {
  protected tableName = 'processed_volunteer_events'

  async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.string('headline').nullable().defaultTo(null)
    })
  }

  async down() {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('headline')
    })
  }
}
