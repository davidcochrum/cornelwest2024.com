import Factory from '@adonisjs/lucid/factories'
import User from '#models/user'

export const UserFactory = Factory.define(User, ({ faker }) => {
  return {
    fullName: faker.person.fullName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
  }
}).build()
