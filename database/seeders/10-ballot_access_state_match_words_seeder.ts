import { BaseSeeder } from '@adonisjs/lucid/seeders'
import BallotAccessState from '#models/ballot_access_state'

// noinspection JSUnusedGlobalSymbols
export default class extends BaseSeeder {
  async run() {
    await BallotAccessState.updateOrCreateMany('code', [
      {
        code: 'AL',
        matchWords: ['al', 'alabama'],
      },
      {
        code: 'AK',
        matchWords: ['ak', 'alaska'],
      },
      {
        code: 'AZ',
        matchWords: ['az', 'arizona'],
      },
      {
        code: 'AR',
        matchWords: ['ar', 'arkansas'],
      },
      {
        code: 'CA',
        matchWords: ['ca', 'california'],
      },
      {
        code: 'CO',
        matchWords: ['co', 'colorado'],
      },
      {
        code: 'CT',
        matchWords: ['ct', 'connecticut'],
      },
      {
        code: 'DC',
        matchWords: ['dc', 'd.c.'],
      },
      {
        code: 'DE',
        matchWords: ['de', 'delaware'],
      },
      {
        code: 'FL',
        matchWords: ['fl', 'florida'],
      },
      {
        code: 'GA',
        matchWords: ['ga', 'georgia'],
      },
      {
        code: 'HI',
        matchWords: ['hawaii'],
      },
      {
        code: 'ID',
        matchWords: ['id', 'idaho'],
      },
      {
        code: 'IL',
        matchWords: [ 'il', 'illinois' ],
      },
      {
        code: 'IN',
        matchWords: ['indiana'],
      },
      {
        code: 'IA',
        matchWords: ['ia', 'iowa'],
      },
      {
        code: 'KS',
        matchWords: ['ks', 'kansas'],
      },
      {
        code: 'KY',
        matchWords: ['ky', 'kentucky'],
      },
      {
        code: 'LA',
        matchWords: ['la', 'louisiana'],
      },
      {
        code: 'ME',
        matchWords: ['maine'],
      },
      {
        code: 'MD',
        matchWords: ['md', 'maryland'],
      },
      {
        code: 'MA',
        matchWords: ['ma', 'massachusetts'],
      },
      {
        code: 'MI',
        matchWords: ['mi', 'michigan'],
      },
      {
        code: 'MN',
        matchWords: ['mn', 'minnesota'],
      },
      {
        code: 'MS',
        matchWords: ['ms', 'mississippi'],
      },
      {
        code: 'MO',
        matchWords: ['mo', 'missouri'],
      },
      {
        code: 'MT',
        matchWords: ['mt', 'montana'],
      },
      {
        code: 'NE',
        matchWords: ['ne', 'nebraska'],
      },
      {
        code: 'NV',
        matchWords: ['nv', 'nevada'],
      },
      {
        code: 'NH',
        matchWords: ['nh', 'new hampshire'],
      },
      {
        code: 'NJ',
        matchWords: ['nj', 'new jersey'],
      },
      {
        code: 'NM',
        matchWords: ['nm', 'new mexico'],
      },
      {
        code: 'NY',
        matchWords: ['ny', 'new york'],
      },
      {
        code: 'NC',
        matchWords: ['nc', 'north carolina'],
      },
      {
        code: 'ND',
        matchWords: ['nd', 'north dakota'],
      },
      {
        code: 'OH',
        matchWords: ['ohio'],
      },
      {
        code: 'OK',
        matchWords: ['oklahoma'],
      },
      {
        code: 'OR',
        matchWords: ['oregon'],
      },
      {
        code: 'PA',
        matchWords: ['pa', 'penn', 'pennsylvania'],
      },
      {
        code: 'RI',
        matchWords: ['ri', 'rhode island'],
      },
      {
        code: 'SC',
        matchWords: ['sc', 'south carolina'],
      },
      {
        code: 'SD',
        matchWords: ['sd', 'south dakota'],
      },
      {
        code: 'TN',
        matchWords: ['tn', 'tenn', 'tennessee'],
      },
      {
        code: 'TX',
        matchWords: ['tx', 'texas'],
      },
      {
        code: 'UT',
        matchWords: ['ut', 'utah'],
      },
      {
        code: 'VT',
        matchWords: ['vt', 'vermont'],
      },
      {
        code: 'VA',
        matchWords: ['va', 'virginia'],
      },
      {
        code: 'WA',
        matchWords: ['wa', 'washington'],
      },
      {
        code: 'WV',
        matchWords: ['wv', 'west virginia'],
      },
      {
        code: 'WI',
        matchWords: ['wi', 'wisconsin'],
      },
      {
        code: 'WY',
        matchWords: ['wy', 'wyoming'],
      },
    ])
  }
}
